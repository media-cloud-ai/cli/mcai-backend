use std::fmt::{self, Display, Formatter};

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
  MissingParameter(String),
  ReqwestError(reqwest::Error),
  SerdeJsonError(serde_json::Error),
}

impl std::error::Error for Error {}

impl From<reqwest::Error> for Error {
  fn from(error: reqwest::Error) -> Self {
    Error::ReqwestError(error)
  }
}

impl From<serde_json::Error> for Error {
  fn from(error: serde_json::Error) -> Self {
    Error::SerdeJsonError(error)
  }
}

impl Display for Error {
  fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
    match self {
      Error::MissingParameter(parameter) => write!(f, "Missing parameter: {}", parameter),
      Error::ReqwestError(error) => write!(f, "HTTP request: {}", error),
      Error::SerdeJsonError(error) => write!(f, "JSON: {}", error),
    }
  }
}
