use super::Login;
use crate::{constants::*, Error, Result};
use std::{convert::TryFrom, str::FromStr};

#[derive(Clone)]
pub struct Configuration {
  pub hostname: String,
  pub port: u16,
  username: String,
  password: String,
}

impl Configuration {
  pub fn get_session_url(&self) -> String {
    self.build_url("/api/sessions")
  }

  pub fn build_url(&self, url: &str) -> String {
    let endpoint = match self.port {
      80 => format!("http://{}", self.hostname),
      443 => format!("https://{}", self.hostname),
      _ => format!("http://{}:{}", self.hostname, self.port),
    };

    format!("{}{}", endpoint, url)
  }
}

impl<'a> TryFrom<&Option<&clap::ArgMatches<'a>>> for Configuration {
  type Error = crate::Error;

  fn try_from(matches: &Option<&clap::ArgMatches<'a>>) -> std::result::Result<Self, Self::Error> {
    let hostname = get_parameter(matches, PARAMETER_BACKEND_HOSTNAME)?;
    let port = get_port(matches)?;
    let username = get_parameter(matches, PARAMETER_BACKEND_USERNAME)?;
    let password = get_parameter(matches, PARAMETER_BACKEND_PASSWORD)?;

    Ok(Configuration {
      hostname,
      port,
      username,
      password,
    })
  }
}

fn get_parameter<'a>(matches: &Option<&clap::ArgMatches<'a>>, key: &str) -> Result<String> {
  matches
    .map(|matches| matches.value_of(key).map(|v| v.to_owned()))
    .flatten()
    .ok_or_else(|| Error::MissingParameter(key.to_string()))
}

fn get_port<'a>(matches: &Option<&clap::ArgMatches<'a>>) -> Result<u16> {
  matches
    .map(|matches| {
      matches
        .value_of(PARAMETER_BACKEND_PORT)
        .map(|value| u16::from_str(value).unwrap())
    })
    .flatten()
    .ok_or_else(|| Error::MissingParameter(PARAMETER_BACKEND_PORT.to_string()))
}

impl From<&Configuration> for Login {
  fn from(configuration: &Configuration) -> Self {
    Login::new(&configuration.username, &configuration.password)
  }
}
