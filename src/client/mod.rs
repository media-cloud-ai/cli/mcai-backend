mod access_token;
mod configuration;
mod login;
mod session;
mod user;

use crate::{Action, Output, Result};
use access_token::AccessToken;
pub use configuration::Configuration;
use login::Login;
use reqwest::{blocking::Client as ReqwestClient, header::CONTENT_TYPE};
use session::Session;
use user::User;

pub struct Client {
  client: ReqwestClient,
  token: Option<String>,
  configuration: Configuration,
}

impl Client {
  pub fn new(configuration: &Configuration) -> Result<Self> {
    let client = ReqwestClient::new();
    let configuration = configuration.clone();

    Ok(Client {
      client,
      token: None,
      configuration,
    })
  }

  fn get_authentication_token(&mut self) -> Result<String> {
    if let Some(token) = &self.token {
      return Ok(token.clone());
    }

    let login_body = Login::from(&self.configuration).to_string();

    let token_response: AccessToken = self
      .client
      .post(self.configuration.get_session_url())
      .header(CONTENT_TYPE, "application/json")
      .body(login_body)
      .send()?
      .json()?;

    self.token = Some(token_response.access_token.clone());
    Ok(token_response.access_token)
  }

  pub fn call(&mut self, action: &Action, output: Output) -> Result<()> {
    let url = self.configuration.build_url(&action.get_url());

    let response = self
      .client
      .request(action.get_method(), url)
      .bearer_auth(self.get_authentication_token()?)
      .send()?;

    match output {
      Output::Formatted => {
        action.format_output(response)?;
      }
      Output::Json(false) => {
        let content: serde_json::Value = response.json()?;
        println!("{}", serde_json::to_string(&content)?);
      }
      Output::Json(true) => {
        let content: serde_json::Value = response.json()?;
        println!("{}", serde_json::to_string_pretty(&content)?);
      }
      Output::Raw => {
        println!("{}", response.text()?);
      }
    }

    Ok(())
  }
}
