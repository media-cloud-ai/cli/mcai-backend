use serde::Serialize;

#[derive(Debug, Default, Serialize)]
pub struct Session {
  pub email: String,
  pub password: String,
}
