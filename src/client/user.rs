use serde::{Deserialize, Serialize};

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct User {
  pub email: String,
  pub id: u32,
  pub rights: Vec<String>,
}
