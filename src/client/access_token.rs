use super::User;
use serde::{Deserialize, Serialize};

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct AccessToken {
  pub access_token: String,
  pub user: User,
}
