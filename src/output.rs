use crate::{Error, Result};
use std::convert::TryFrom;

type Pretty = bool;

pub enum Output {
  Formatted,
  Json(Pretty),
  Raw,
}

impl<'a> TryFrom<&Option<&clap::ArgMatches<'a>>> for Output {
  type Error = crate::Error;

  fn try_from(matches: &Option<&clap::ArgMatches<'a>>) -> std::result::Result<Self, Self::Error> {
    let output = match get_parameter(matches, "output")?.as_str() {
      "json" => Output::Json(false),
      "pretty-json" => Output::Json(true),
      "formatted" => Output::Formatted,
      _ => Output::Raw,
    };

    Ok(output)
  }
}

fn get_parameter<'a>(matches: &Option<&clap::ArgMatches<'a>>, key: &str) -> Result<String> {
  matches
    .map(|matches| matches.value_of(key).map(|v| v.to_owned()))
    .flatten()
    .ok_or_else(|| Error::MissingParameter(key.to_string()))
}
