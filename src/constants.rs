pub mod built_info {
  include!(concat!(env!("OUT_DIR"), "/built.rs"));
}

pub static SUBCOMMAND_WORKFLOW_DEFINITIONS: &str = "workflow-definitions";
pub static SUBCOMMAND_WORKER_DEFINITIONS: &str = "worker-definitions";
pub static SUBCOMMAND_WORKFLOWS: &str = "workflows";

pub static SUBCOMMAND_WORKFLOW_DEFINITION: &str = "workflow-definition";
pub static SUBCOMMAND_WORKER_DEFINITION: &str = "worker-definition";
pub static SUBCOMMAND_WORKFLOW: &str = "workflow";

pub static PARAMETER_BACKEND_HOSTNAME: &str = "backend-hostname";
pub static PARAMETER_BACKEND_PORT: &str = "backend-port";
pub static PARAMETER_BACKEND_USERNAME: &str = "backend-username";
pub static PARAMETER_BACKEND_PASSWORD: &str = "backend-password";

pub static PARAMETER_IDENTIFIER: &str = "identifier";
pub static PARAMETER_OUTPUT: &str = "output";
