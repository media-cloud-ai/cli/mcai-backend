use serde::Deserialize;
use term_table::{row::Row, table_cell::TableCell};

#[derive(Deserialize)]
pub struct WorkerDefinition {
  created_at: String,
  id: u32,
  label: String,
  queue_name: String,
  short_description: String,
  version: String,
}

impl<'a> From<&WorkerDefinition> for Row<'a> {
  fn from(worker: &WorkerDefinition) -> Row<'a> {
    Row::new(vec![
      TableCell::new(format!("{}", worker.id)),
      TableCell::new(&worker.created_at),
      TableCell::new(&worker.label),
      TableCell::new(&worker.version),
      TableCell::new(&worker.queue_name),
      TableCell::new(&worker.short_description),
    ])
  }
}

pub fn worker_definitions_columns<'a>() -> Row<'a> {
  Row::new(vec![
    TableCell::new("ID"),
    TableCell::new("Created at"),
    TableCell::new("Label"),
    TableCell::new("Version"),
    TableCell::new("Queue name"),
    TableCell::new("Short description"),
  ])
}
