use super::{workflow::workflow_columns, Workflow};
use serde::Deserialize;
use term_table::{
  row::Row,
  table_cell::{Alignment, TableCell},
  Table, TableStyle,
};

#[derive(Deserialize)]
pub struct WorkflowsData {
  data: Vec<Workflow>,
  total: usize,
}

impl WorkflowsData {
  pub fn render(&self) {
    let mut table = Table::new();
    table.style = TableStyle::rounded();
    table.add_row(workflow_columns());

    for workflow in &self.data {
      table.add_row(workflow.into());
    }

    table.add_row(Row::new(vec![TableCell::new_with_alignment(
      format!("{}-{}/{}", 0, self.data.len(), self.total),
      6,
      Alignment::Right,
    )]));

    println!("{}", table.render());
  }
}
