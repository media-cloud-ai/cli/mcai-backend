use serde::Deserialize;
use term_table::{row::Row, table_cell::TableCell};

#[derive(Deserialize)]
pub struct Workflow {
  created_at: String,
  id: u32,
  identifier: String,
  reference: Option<String>,
  schema_version: String,
  version_major: u32,
  version_minor: u32,
  version_micro: u32,
}

impl<'a> From<&Workflow> for Row<'a> {
  fn from(workflow: &Workflow) -> Row<'a> {
    Row::new(vec![
      TableCell::new(format!("{}", workflow.id)),
      TableCell::new(&workflow.created_at),
      TableCell::new(&workflow.identifier),
      TableCell::new(format!(
        "{}.{}.{}",
        workflow.version_major, workflow.version_minor, workflow.version_micro
      )),
      TableCell::new(&workflow.schema_version),
      TableCell::new(workflow.reference.clone().unwrap_or_default()),
    ])
  }
}

pub fn workflow_columns<'a>() -> Row<'a> {
  Row::new(vec![
    TableCell::new("ID"),
    TableCell::new("Created at"),
    TableCell::new("Identifier"),
    TableCell::new("Version"),
    TableCell::new("Schema Version"),
    TableCell::new("Reference"),
  ])
}
