use super::{worker_definition::worker_definitions_columns, WorkerDefinition};
use serde::Deserialize;
use term_table::{
  row::Row,
  table_cell::{Alignment, TableCell},
  Table, TableStyle,
};

#[derive(Deserialize)]
pub struct WorkerDefinitionsData {
  data: Vec<WorkerDefinition>,
  total: usize,
}

impl WorkerDefinitionsData {
  pub fn render(&self) {
    let mut table = Table::new();
    table.style = TableStyle::rounded();
    table.add_row(worker_definitions_columns());

    for worker in &self.data {
      table.add_row(worker.into());
    }

    table.add_row(Row::new(vec![TableCell::new_with_alignment(
      format!("{}-{}/{}", 0, self.data.len(), self.total),
      6,
      Alignment::Right,
    )]));

    println!("{}", table.render());
  }
}
