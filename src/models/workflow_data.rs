use super::{workflow::workflow_columns, Workflow};
use serde::Deserialize;
use term_table::{Table, TableStyle};

#[derive(Deserialize)]
pub struct WorkflowData {
  data: Workflow,
}

impl WorkflowData {
  pub fn render(&self) {
    let mut table = Table::new();
    table.style = TableStyle::rounded();
    table.add_row(workflow_columns());
    table.add_row((&self.data).into());
    println!("{}", table.render());
  }
}
