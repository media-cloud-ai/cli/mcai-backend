use super::{worker_definition::worker_definitions_columns, WorkerDefinition};
use serde::Deserialize;
use term_table::{Table, TableStyle};

#[derive(Deserialize)]
pub struct WorkerDefinitionData {
  data: WorkerDefinition,
}

impl WorkerDefinitionData {
  pub fn render(&self) {
    let mut table = Table::new();
    table.style = TableStyle::rounded();
    table.add_row(worker_definitions_columns());
    table.add_row((&self.data).into());
    println!("{}", table.render());
  }
}
