use clap::{App, Arg, SubCommand};
use mcai_backend::{constants::*, Action, Client, ClientConfiguration, Output, Result};
use std::convert::TryFrom;

fn main() -> Result<()> {
  let backend_hostname = Arg::with_name("backend-hostname")
    .long(PARAMETER_BACKEND_HOSTNAME)
    .env("BACKEND_HOSTNAME");

  let backend_port = Arg::with_name("backend-port")
    .long(PARAMETER_BACKEND_PORT)
    .env("BACKEND_PORT");

  let backend_username = Arg::with_name("backend-username")
    .long(PARAMETER_BACKEND_USERNAME)
    .env("BACKEND_USERNAME");

  let backend_password = Arg::with_name("backend-password")
    .long(PARAMETER_BACKEND_PASSWORD)
    .env("BACKEND_PASSWORD");

  let identifier_argument = Arg::with_name(PARAMETER_IDENTIFIER)
    .index(1)
    .takes_value(true)
    .required(true)
    .help("Identifier of the item");

  let output_mode = Arg::with_name(PARAMETER_OUTPUT)
    .short("o")
    .possible_values(&["formatted", "raw", "json", "pretty-json"])
    .default_value("formatted")
    .help("Configure the rendering output");

  let common_args = vec![
    backend_hostname,
    backend_port,
    backend_username,
    backend_password,
    output_mode,
  ];

  let matches = App::new(built_info::PKG_NAME)
    .about("Interact with Media Cloud AI backend")
    .version(built_info::PKG_VERSION)
    .subcommand(
      SubCommand::with_name(SUBCOMMAND_WORKFLOW_DEFINITIONS)
        .about("List all workflows definitions")
        .version(built_info::PKG_VERSION)
        .args(&common_args),
    )
    .subcommand(
      SubCommand::with_name(SUBCOMMAND_WORKFLOW_DEFINITION)
        .about("Get identified workflow definition")
        .version(built_info::PKG_VERSION)
        .args(&common_args)
        .arg(identifier_argument.clone()),
    )
    .subcommand(
      SubCommand::with_name(SUBCOMMAND_WORKER_DEFINITIONS)
        .about("List all worker definitions")
        .version(built_info::PKG_VERSION)
        .args(&common_args),
    )
    .subcommand(
      SubCommand::with_name(SUBCOMMAND_WORKER_DEFINITION)
        .about("Get worker workflow information")
        .version(built_info::PKG_VERSION)
        .args(&common_args)
        .arg(identifier_argument.clone()),
    )
    .subcommand(
      SubCommand::with_name(SUBCOMMAND_WORKFLOWS)
        .about("Get workflows informations")
        .version(built_info::PKG_VERSION)
        .args(&common_args),
    )
    .subcommand(
      SubCommand::with_name(SUBCOMMAND_WORKFLOW)
        .about("Get identified workflow informations")
        .version(built_info::PKG_VERSION)
        .args(&common_args)
        .arg(identifier_argument.clone()),
    )
    .get_matches();

  let (_, subcommand_parameters) = matches.subcommand();
  let client_configuration = ClientConfiguration::try_from(&subcommand_parameters)?;
  let output = Output::try_from(&subcommand_parameters)?;

  let mut client = Client::new(&client_configuration)?;

  let action = Action::try_from(&matches)?;

  client.call(&action, output)?;

  Ok(())
}
